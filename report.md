# Student: Le Gia Duy

## Overview:
In this project, I use **Typescripts** to build both client and server codes.

**Typescripts** are **transpiled** into **Javascripts** so that it can run well with **Node-js** naturally.

## Maturity of the features:
I have implemented all requirements of the project as followings:

#### Commands/Controllers:
Besides required commands, I added more commands to suite my design which helps increasing total commands up to **21**.

User can type `help;` to print out all commands that he/she can use to interact with server (after a successful login).

All the commands and messages exchanged between server and client come in this form:

| Format of the exchanged messages
|:---------------:|
|``` {action: '', sender: '', msg: '', group: '', destination: '', reason: '', id: ''}```|

Following commands are implemented and besides ``help``, all of them have a corresponding controllers at server side to handle requests and return responses:
* `help`: "Print all commands and their descriptions"
* `b`: "Send messages to all current connected users (note: these messages are anonymous, therefore are not persisted to server)"
* `s`: "Send private messages to an user"
* `pwd`: "Request to change new password"
* `ls`: "List current connected users"
* `q`: "Disconnect from the server (logout)"
* `cg`: "Create a new (not yet existed) group and grant the current user as owner and the first member of the group"
* `bg`: "Send messages to all current connected users of a group (note: accessible only for members of the group)"
* `invite`: "Add an user to a group (note: accessible only for admin/owner of the group)"
* `j`: "Join an existing group"
* `leave`: "Leave an existing group"
* `kick`: "Kick an user from a group (note: accessible only for admin/owner of the group)"
* `members`: "List all users of a group"
* `groups`: "List all current existing groups"
* `states`: "List all events happened in a group (note: excluding messages exchanged between users)"
* `messages`: "List only messages exchanged in a group (note: excluding other group events between users)"
* `addadm`: "Grant admin role to an user in a group (note: accessible only for admin/owner of the group)"
* `rmadm`: "Remove admin role of an user in a group (note: accessible only for admin/owner of the group)"
* `ban`: "Ban an user from a group (note: accessible only for admin/owner of the group)"
* `bans`: "List all banned users of a group"
* `unban`: "Un-ban an user from a group(note: accessible only for admin/owner of the group)"

#### Groups and users management:
TChat application is not only responsible for broadcasting messages between connected users/sockets.

It also has features to group users and exchange messages only in this group.

File **GroupService.ts** will handle most of tasks related to: 
- Adding/removing users upon their joins/leaves/kicked.
- Banning/un-banning users based on requests of Admin/Owner users.
- Storing and managing messages exchanged in the group.
- Displaying events happened in the group when requested.
- Promoting users to admin role or demoting them from this role.
- Listing 

File **UserService.ts** will handle most of tasks related to users and their connections:
- Signing up new users (first time username connecting server).
- Authenticating existing users.
- Sending message to specific users.
- Disconnecting users.
- Storing user information such as id, password, and secret key.
- List connected users.

User's role in a group:
- Member (lowest): can broadcast messages to other group's members and invite others to the group.
- Admin (medium): can do more like add/remove a another admin, kick/ban/un-ban an user.
- Owner (highest): can do anything.

#### Messages transfers security:
To secure messages exchanged between client and server, there must be some mechanism to protect the data.
Followings are implemented:
- User authentication using passwords.
- Passwords are store in hashed string to database to avoid hacking.
- Messages are encrypted using asymmetric keys with advanced cipher technique such as initialization vector.

Steps occurred during secured messaging:
1. Client asks for username and password.
2. Client generates a public/private keys pair.
3. Client attempts a connection to server with its public key.
4. Server listens on connection.
5. Server generates another public/private keys pair for that client.
6. Server sends its public key to client and compute a secret.
7. Client receives server's public key and compute a secret.
8. Client encrypt user's information and sends to server for authentication.
9. Servers check and ensures user's authenticity. 
10. If user fails then it is disconnected.
11. If user passes then the channel is secured for all future messages.

#### Persistent:
TChat application uses sqlite to durably persists all information to a local database file.

All information exchanged between client and servers are recorded including:
- Users
- Groups
- Messages/States

File **DatabaseService.ts** will be responsible for opening/creating a database both physically or in memory.
Jobs of this service:
- Saving user data.
- Saving group data.
- Saving relationships between users and groups.
- Saving messages.
- Saving relationsips between users, groups and messages.

|Database schema:
|:---------------:|
 ```
 DROP TABLE IF EXISTS "users";
 CREATE TABLE IF NOT EXISTS "users" (
     "user_id"       TEXT NOT NULL PRIMARY KEY UNIQUE,
     "pwd"           TEXT NOT NULL
 );
 DROP TABLE IF EXISTS "groups";
 CREATE TABLE IF NOT EXISTS "groups" (
     "group_id"      TEXT NOT NULL PRIMARY KEY UNIQUE,
     "is_private"    INTEGER
 );
 DROP TABLE IF EXISTS "user_groups";
 CREATE TABLE IF NOT EXISTS "user_groups" (
     "user_id"       TEXT NOT NULL,
     "group_id"      TEXT NOT NULL,
     "role"          INTEGER NOT NULL DEFAULT 1,
     "is_ban"        INTEGER NOT NULL DEFAULT 0,
     FOREIGN KEY(user_id) references users("user_id"),
     FOREIGN KEY(group_id) references "groups"("group_id")
 );
 DROP TABLE IF EXISTS "messages";
 CREATE TABLE IF NOT EXISTS "messages" (
     "message_id"    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
     "user_id"       TEXT NOT NULL,
     "action"        TEXT NOT NULL,
     "msg"           TEXT,
     "group_id"      TEXT,
     "destination"   TEXT,
     "reason"        TEXT,
     FOREIGN KEY(user_id) references users("user_id"),
     FOREIGN KEY(group_id) references "groups"("group_id")
 );
 ```