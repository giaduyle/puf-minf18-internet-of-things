# Build:
`npm run-script clean && npm run-script build`
- Clean the _tchat/dist_ directory if existed.
- Build typescripts files into javascripts files and placed them into _tchat/dist_.

# Run
Server
- Navigate to the _tchat/dist/server_.
- `node Server.js`.
- By default, a database file named **TChat.db** will be created at the folder of **Server.js**.

Client
- Navigate to the _tchat/dist/client_.
- `node Client.js`.
- After login successfully with server, user can type `help;` to print out all supported commands and their descriptions.

# Tests
Unit tests
- `npm run-script test:unit`

Integration tests
- `npm run-script test:integration`

# Development
Server
- `npm run-script dev-server`
- This will start **Server.ts** file in watch mode (with changes auto-detected and reloaded).

Client
- `npm run-script dev-client`
- This will start **Client.ts** file in watch mode (with changes auto-detected and reloaded).

##### Note: All of the above commands must be run at _/tchat_ folder.