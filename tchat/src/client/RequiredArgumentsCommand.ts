import Command from "./Command";

export default abstract class RequiredArgumentsCommand extends Command {
    validateArguments(): void {
        if (this.getArgs() == undefined || this.getArgs().length == 0) {
            throw Error("Empty arguments!");
        }
    }
}