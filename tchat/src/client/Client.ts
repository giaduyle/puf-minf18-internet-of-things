import ConnectionOptions from './ConnectionOptions';
import * as SocketIO from 'socket.io-client';
import Message from '../common/Message';
import UserCommandFacade from "./UserCommandFacade";
import CommandListener from "./CommandListener";
import Command from "./Command";
import User from "../common/User";
import {createInterface} from "readline";
import Security from "../common/Security";
import {ECDH} from "crypto";

class Client implements CommandListener{
    private readonly connectionOptions: ConnectionOptions;
    private readonly commandFacade: UserCommandFacade;

    private socket: SocketIOClient.Socket;
    private currentUser: User;
    private secureChannel: ECDH;
    private secret: Buffer | string;

    constructor(connectionOptions: ConnectionOptions) {
        this.connectionOptions = connectionOptions;
        this.commandFacade = new UserCommandFacade(
            createInterface({input: process.stdin, output: process.stdout}));
        this.commandFacade.addListener(this);
    }

    public async start() {
        await this.commandFacade.requestUser(user => {
            this.currentUser = user;
            this.connectToServer();
            this.listenOnConnectionEvents();
            this.listenOnMessages();
            this.listenOnBroadcasts();
            this.listenOnUserInputs();
        });
    }

    private connectToServer() {
        let serverUri = this.connectionOptions.getUri();
        console.log('Connecting to server: ' + serverUri);
        let publicKey = this.generatePublicKey();
        this.socket = SocketIO.connect(serverUri, {
            query: `publicKey=${publicKey}`
        });
        this.socket.on("server-public-key", (serverKey) => {
            this.secret = this.secureChannel.computeSecret(serverKey, Security.DEFAULT_OUTPUT_CODING);
            let encrypted = Security.encrypt(this.currentUser.toJson(), this.secret);
            this.socket.emit("login", encrypted);
        });
    }

    private generatePublicKey() {
        this.secureChannel = Security.createECDHChannel();
        return this.secureChannel.generateKeys(Security.DEFAULT_OUTPUT_CODING);
    }

    private listenOnUserInputs() {
        this.commandFacade.listenOnUserInputs();
    }

    onCommand(command: Command): void {
        try {
            command.validateArguments();
            this.send(command.toMessage());
        } catch (error) {
            console.log(error.message);
        }
    }

    private listenOnConnectionEvents() {
        this.socket.on("connect", () => {
            console.log("Connected to server!");
        });
        this.socket.on("connect_error", () => {
            console.log("Cannot connect to server!");
        });
    }

    private listenOnMessages() {
        this.socket.on("message", (data) => {
            if (this.secret == undefined) {
                return;
            }
            let decrypted = Security.decrypt(data, this.secret);
            let message = Message.fromJson(JSON.parse(decrypted));
            console.log("<" + message.getSender() + ">: " + message.getMessage());
        });
    }

    private listenOnBroadcasts() {
        this.socket.on("broadcast", (data) => {
            if (this.secret == undefined) {
                return;
            }
            let message = Message.fromJson(JSON.parse(data));
            console.log("<" + message.getSender() + ">: " + message.getMessage());
        });
    }

    private send(message: Message) {
        if (this.secret == undefined) {
            console.log("Server did not exchange keys!");
            return;
        }
        message.setSender(this.currentUser.getId());
        this.socket.send(Security.encrypt(message.toJson(), this.secret));
    }
}

export const main = async (): Promise<void> => {
    return new Promise<void>(async () => {
        await new Client(new ConnectionOptions('localhost', 3636)).start();
    });
};
main().catch(error => console.log(error));