import Command from "./Command";

export default interface CommandListener {
    onCommand(cmd: Command): void;
}