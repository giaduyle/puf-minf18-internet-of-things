import Command from "./Command";
import AddGroupAdmin from "./commands/AddGroupAdmin";
import BanFromGroup from "./commands/BanFromGroup";
import BroadcastAll from "./commands/BroadcastAll";
import BroadcastGroup from "./commands/BroadcastGroup";
import ChangePassword from "./commands/ChangePassword";
import CreateGroup from "./commands/CreateGroup";
import Help from "./commands/Help";
import InviteToGroup from "./commands/InviteToGroup";
import JoinGroup from "./commands/JoinGroup";
import KickFromGroup from "./commands/KickFromGroup";
import LeaveGroup from "./commands/LeaveGroup";
import ListGroupBans from "./commands/ListGroupBans";
import ListGroupMembers from "./commands/ListGroupMembers";
import ListGroups from "./commands/ListGroups";
import ListGroupStates from "./commands/ListGroupStates";
import ListMessages from "./commands/ListMessages";
import ListUsers from "./commands/ListUsers";
import Quit from "./commands/Quit";
import RemoveGroupAdmin from "./commands/RemoveGroupAdmin";
import SendPrivately from "./commands/SendPrivately";
import UnbanFromGroup from "./commands/UnbanFromGroup";

export default class CommandRegistry {
    private static readonly SUPPORTED_COMMANDS: Command[] = [
        new AddGroupAdmin(),
        new BanFromGroup(),
        new BroadcastAll(),
        new BroadcastGroup(),
        new ChangePassword(),
        new CreateGroup(),
        new Help(),
        new InviteToGroup(),
        new JoinGroup(),
        new KickFromGroup(),
        new LeaveGroup(),
        new ListGroupBans(),
        new ListGroupMembers(),
        new ListGroups(),
        new ListGroupStates(),
        new ListMessages(),
        new ListUsers(),
        new Quit(),
        new RemoveGroupAdmin(),
        new SendPrivately(),
        new UnbanFromGroup(),
    ];

    private commands: Map<string, Command> = new Map<string, Command>();

    public static readonly DEFAULT_INSTANCE: CommandRegistry =
        new CommandRegistry(CommandRegistry.SUPPORTED_COMMANDS);

    constructor(controllers: Command[]) {
        controllers.forEach(command => {
            if (this.commands.has(command.getPattern())) {
                throw Error(`Duplicated command pattern: ${command.getPattern()}`)
            }
            this.commands.set(command.getPattern(), command);
        });
    }

    getCommand(pattern: string): Command | undefined {
        return this.commands.get(pattern);
    }

    getAllCommands(): Command[] {
        return Array.from(this.commands.values());
    }
}