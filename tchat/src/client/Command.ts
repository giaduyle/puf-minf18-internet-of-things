import Message from "../common/Message";

export default abstract class Command {
    public static readonly VALID_COMMAND_PATTERN: RegExp = new RegExp('\\w+(;\\w*)+');
    public static readonly DELIMITER: string = ';';

    private readonly pattern: string;
    private readonly action: string;
    private readonly description: string;
    private args: string[] = [];

    protected constructor(pattern: string, action: string, description?: string) {
        this.pattern = pattern;
        this.action = action;
        this.description = description;
    }

    public abstract toMessage(): Message;

    public abstract validateArguments(): void;

    public getPattern(): string {
        return this.pattern;
    }

    public getAction(): string {
        return this.action;
    }

    public getDescription(): string {
        return this.description;
    }

    public getArgs(): string[] {
        return this.args;
    }

    public setArgs(args: string[]) {
        this.args = args;
    }

    public clearArgs() {
        this.args = [];
    }
}