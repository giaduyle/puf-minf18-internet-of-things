import Command from "./Command";
import Message from "../common/Message";
import {checkArgumentsSize} from "../common/ConditionsUtil";

export default abstract class NoArgumentCommand extends Command {
    toMessage(): Message {
        return new Message(this.getAction());
    }

    readonly setArgs = (): void => {
        throw Error("'" + this.getPattern() + Command.DELIMITER + "' does not support arguments!");
    };

    readonly validateArguments = (): void => {
        checkArgumentsSize(this.getArgs(), 0);
    }
}