import Message from "../common/Message";
import NoArgumentCommand from "./NoArgumentCommand";

export default class UnknownCommand extends NoArgumentCommand {
    constructor() {
        super("!?", "unknown");
    }

    public toMessage(): Message {
        throw Error('Unsupported: converting UnknownCommand command to a Message!');
    }
}