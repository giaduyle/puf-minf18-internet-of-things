import {createInterface, Interface} from "readline";
import CommandListener from "./CommandListener";
import Command from "./Command";
import UnknownCommand from "./UnknownCommand";
import User from "../common/User";
import Help from "./commands/Help";
import Quit from "./commands/Quit";
import CommandRegistry from "./CommandRegistry";

export default class UserCommandFacade {
    private readonly listeners: CommandListener[] = [];
    private readonly lineReader: Interface;
    private readonly commandRegistry;

    constructor(lineReader?: Interface) {
        this.lineReader = (lineReader != undefined)
            ? lineReader
            : createInterface({input: process.stdin, output: process.stdout});
        this.commandRegistry = CommandRegistry.DEFAULT_INSTANCE;
    }

    public async requestUser(onUser: (user: User) => void): Promise<User> {
        return new Promise<User>((resolve) => {
            this.lineReader.question('Please enter a nickname: ', (name) => {
                if (name == null || name.length == 0) {
                    console.log('Invalid username: ' + name);
                    return this.requestUser(onUser);
                }
                this.lineReader.question('Please enter a password: ', (pwd) => {
                    if (pwd == null || pwd.length == 0) {
                        console.log('Invalid password: ' + pwd);
                        return this.requestUser(onUser);
                    }
                    let user = new User(name, pwd);
                    onUser(user);
                    resolve(user);
                });
            });
        });
    }

    public listenOnUserInputs() {
        this.lineReader.prompt(true);
        this.lineReader.on("line", (line) => {
            if (!UserCommandFacade.isValidInput(line)) {
                console.log("Invalid command format: " + line);
                return;
            }
            let tokens: string[] = line.split(Command.DELIMITER);
            let cmdToken: string = tokens[0];
            let foundCommand: Command = this.lookupCommand(cmdToken);
            foundCommand.clearArgs();
            if (foundCommand instanceof Help) {
                this.printHelp();
                return;
            }
            if (foundCommand instanceof UnknownCommand) {
                console.log("UnknownCommand command: " + cmdToken);
                return;
            }
            let argsToken: string[] = tokens.splice(1, tokens.length - 1);
            if (argsToken != undefined && argsToken[0].length > 0) {
                foundCommand.setArgs(argsToken);
            }
            this.listeners.forEach(aware => {
                aware.onCommand(foundCommand);
            });
        });
    }

    private printHelp() {
        console.log(
            "Supported commands: \n" +
            this.commandRegistry.getAllCommands().map(
                cmd => `\t>> ${cmd.getPattern()}: "${cmd.getDescription()}"`).join("\n"));
    }

    public addListener(listener: CommandListener): void {
        this.listeners.push(listener);
    }

    public close(): void {
        this.lineReader.close();
    }

    private lookupCommand(cmdToken: string): Command {
        if (cmdToken == undefined || cmdToken.length == 0) {
            return new UnknownCommand();
        }
        return this.commandRegistry.getCommand(cmdToken);
    }

    private static isValidInput(line: string): boolean {
        return line != null && line.match(Command.VALID_COMMAND_PATTERN) != null;
    }
}