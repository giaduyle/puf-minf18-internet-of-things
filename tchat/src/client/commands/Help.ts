import NoArgumentCommand from "../NoArgumentCommand";

export default class Help extends NoArgumentCommand {
    constructor() {
        super("help", "help",
            "Print all commands and their descriptions");
    }
}