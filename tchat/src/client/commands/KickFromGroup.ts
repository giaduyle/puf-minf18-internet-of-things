import RequiredArgumentsCommand from "../RequiredArgumentsCommand";
import Message from "../../common/Message";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class KickFromGroup extends RequiredArgumentsCommand {
    constructor() {
        super("kick", "kick",
            "Kick an user from a group " +
            "(note: accessible only for admin/owner of the group)");
    }
    
    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setGroup(this.getArgs()[0]);
        message.setDestination(this.getArgs()[1]);
        message.setReason(this.getArgs()[2]);
        return message;
    }

    validateArguments(): void {
        super.validateArguments();
        let args = this.getArgs();
        checkArgumentsSize(args, 3);
        checkArgumentNotEmpty(args[0], "Invalid empty group name!");
        checkArgumentNotEmpty(args[1], "Invalid empty user name!");
        checkArgumentNotEmpty(args[2], "Invalid empty reason!");
    }
}