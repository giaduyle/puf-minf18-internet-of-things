import RequiredArgumentsCommand from "../RequiredArgumentsCommand";
import Message from "../../common/Message";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class ListGroupMembers extends RequiredArgumentsCommand {
    constructor() {
        super("members", "members",
            "List all users of a group");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setGroup(this.getArgs()[0]);
        return message;
    }

    validateArguments(): void {
        super.validateArguments();
        let args = this.getArgs();
        checkArgumentsSize(args, 1);
        checkArgumentNotEmpty(args[0], "Invalid empty group name!");
    }
}