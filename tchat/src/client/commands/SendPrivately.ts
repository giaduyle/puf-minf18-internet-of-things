import Message from "../../common/Message";
import RequiredArgumentsCommand from "../RequiredArgumentsCommand";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class SendPrivately extends RequiredArgumentsCommand {
    constructor() {
        super("s", "send",
            "Send private messages to an user");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setDestination(this.getArgs()[0]);
        message.setMessage(this.getArgs()[1]);
        return message;
    }

    validateArguments(): void {
        super.validateArguments();
        let args = this.getArgs();
        checkArgumentsSize(args, 2);
        checkArgumentNotEmpty(args[0], "Invalid empty user name!");
        checkArgumentNotEmpty(args[1], "Invalid empty message!");
    }
}