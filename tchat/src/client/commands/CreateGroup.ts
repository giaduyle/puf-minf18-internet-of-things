import Message from "../../common/Message";
import RequiredArgumentsCommand from "../RequiredArgumentsCommand";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class CreateGroup extends RequiredArgumentsCommand {
    constructor() {
        super("cg", "create group",
            "Create a new (not yet existed) group and grant the current user as owner and the first member of the group");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setGroup(this.getArgs()[0]);
        return message;
    }

    validateArguments(): void {
        super.validateArguments();
        let args = this.getArgs();
        checkArgumentsSize(args, 1);
        checkArgumentNotEmpty(args[0], "Invalid empty group name!");
    }
}