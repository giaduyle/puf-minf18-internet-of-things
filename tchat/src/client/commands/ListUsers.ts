import NoArgumentCommand from "../NoArgumentCommand";

export default class ListUsers extends NoArgumentCommand {
    constructor() {
        super("ls", "list",
            "List current connected users");
    }
}