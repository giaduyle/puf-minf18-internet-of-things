import NoArgumentCommand from "../NoArgumentCommand";

export default class ListGroups extends NoArgumentCommand {
    constructor() {
        super("groups", "groups",
            "List all current existing groups");
    }
}