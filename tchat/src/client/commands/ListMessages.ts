import Message from "../../common/Message";
import {checkArgumentsMaxSize} from "../../common/ConditionsUtil";
import Command from "../Command";

export default class ListMessages extends Command {
    constructor() {
        super("messages", "messages",
            "List only messages exchanged in a group " +
            "(note: excluding other group events between users)");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        let args = this.getArgs();
        if (args != undefined && args.length > 0) {
            message.setGroup(this.getArgs()[0]);
        }
        return message;
    }

    validateArguments(): void {
        let args = this.getArgs();
        checkArgumentsMaxSize(args, 1);
    }
}