import RequiredArgumentsCommand from "../RequiredArgumentsCommand";
import Message from "../../common/Message";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class ChangePassword extends RequiredArgumentsCommand {
    constructor() {
        super("pwd", "change password",
            "Request to change new password");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setMessage(this.getArgs()[0]);
        return message;
    }

    validateArguments(): void {
        super.validateArguments();
        let args = this.getArgs();
        checkArgumentsSize(args, 1);
        checkArgumentNotEmpty(args[0], "Invalid empty new password!");
    }
}