import NoArgumentCommand from "../NoArgumentCommand";

export default class Quit extends NoArgumentCommand {
    constructor() {
        super("q", "quit",
            "Disconnect from the server (logout)");
    }
}