import Message from "../../common/Message";
import RequiredArgumentsCommand from "../RequiredArgumentsCommand";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class BroadcastGroup extends RequiredArgumentsCommand {
    constructor() {
        super("bg", "broadcast group",
            "Send messages to all current connected users of a group " +
            "(note: accessible only for members of the group)");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setGroup(this.getArgs()[0]);
        message.setMessage(this.getArgs()[1]);
        return message;
    }

    validateArguments(): void {
        super.validateArguments();
        let args = this.getArgs();
        checkArgumentsSize(args, 2);
        checkArgumentNotEmpty(args[0], "Invalid empty group name!");
        checkArgumentNotEmpty(args[1], "Invalid empty message!");
    }
}