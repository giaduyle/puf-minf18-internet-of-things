import Command from "../Command";
import Message from "../../common/Message";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class BroadcastAll extends Command {
    constructor() {
        super("b", "broadcast",
            "Send messages to all current connected users " +
            "(note: these messages are anonymous, therefore are not persisted to server)");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setMessage(this.getArgs()[0]);
        return message;
    }

    validateArguments(): void {
        let args = this.getArgs();
        checkArgumentsSize(args, 1);
        checkArgumentNotEmpty(args[0], "Invalid empty message!");
    }
}