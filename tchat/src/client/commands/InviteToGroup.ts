import RequiredArgumentsCommand from "../RequiredArgumentsCommand";
import Message from "../../common/Message";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class InviteToGroup extends RequiredArgumentsCommand {
    constructor() {
        super("invite", "invite",
            "Add an user to a group " +
            "(note: accessible only for admin/owner of the group)");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setGroup(this.getArgs()[0]);
        message.setDestination(this.getArgs()[1]);
        return message;
    }

    validateArguments(): void {
        super.validateArguments();
        let args = this.getArgs();
        checkArgumentsSize(args, 2);
        checkArgumentNotEmpty(args[0], "Invalid empty group name!");
        checkArgumentNotEmpty(args[1], "Invalid empty user name!");
    }
}