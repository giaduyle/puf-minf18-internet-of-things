import RequiredArgumentsCommand from "../RequiredArgumentsCommand";
import Message from "../../common/Message";
import {checkArgumentNotEmpty, checkArgumentsSize} from "../../common/ConditionsUtil";

export default class LeaveGroup extends RequiredArgumentsCommand {
    constructor() {
        super("leave", "leave",
            "Leave an existing group");
    }

    toMessage(): Message {
        let message = new Message(this.getAction());
        message.setGroup(this.getArgs()[0]);
        return message;
    }

    validateArguments(): void {
        super.validateArguments();
        let args = this.getArgs();
        checkArgumentsSize(args, 1);
        checkArgumentNotEmpty(args[0], "Invalid empty group name!");
    }
}