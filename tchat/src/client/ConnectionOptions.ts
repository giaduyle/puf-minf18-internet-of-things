export default class ConnectionOptions {
    private _host: string;
    private _port: number;

    constructor(host: string, port: number) {
        this._host = host;
        this._port = port;
    }

    public getUri(): string {
        return 'http://' + this.host + ':' + this.port;
    }

    get host(): string {
        return this._host;
    }

    set host(value: string) {
        this._host = value;
    }

    get port(): number {
        return this._port;
    }

    set port(value: number) {
        this._port = value;
    }
}