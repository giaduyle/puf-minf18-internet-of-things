import Message from "./Message";

export const prettifyMessagesToLines = (messages: Message[]): string => {
    return messages.map(m => m.toJson()).join("\n");
};