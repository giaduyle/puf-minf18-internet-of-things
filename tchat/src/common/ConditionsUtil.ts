export const checkArgumentsSize = (args: any[], expectedSize: number) => {
    if (args == undefined ||  args.length != expectedSize) {
        throw Error("Invalid arguments. Expected: " + expectedSize);
    }
};

export const checkArgumentsMaxSize = (args: any[], expectedMaxSize: number) => {
    if (args == undefined || args.length > expectedMaxSize) {
        throw Error("Invalid arguments. Expected maximum: " + expectedMaxSize);
    }
};

export const checkArgumentNotEmpty = (arg: any, reason: string) => {
    if (arg == undefined || arg.length == 0) {
        throw Error(reason);
    }
};
