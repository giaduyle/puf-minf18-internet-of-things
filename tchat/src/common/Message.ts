export default class Message {
    private action: string;
    private sender?: string;
    private msg?: string;
    private group?: string;
    private destination?: string;
    private reason?: string;
    private id?: number;

    public static fromJson(json: any): Message {
        if (json.action == undefined) {
            throw new Error("Illegal argument: the given json is malformed or not containing 'action' field: " + json);
        }
        let parsed: Message = new Message(json.action);
        if (json.sender != undefined) {
            parsed.sender = json.sender;
        }
        if (json.msg != undefined) {
            parsed.msg = json.msg;
        }
        if (json.group != undefined) {
            parsed.group = json.group;
        }
        if (json.destination != undefined) {
            parsed.destination = json.destination;
        }
        if (json.reason != undefined) {
            parsed.reason = json.reason;
        }
        if (json.id != undefined) {
            parsed.id = json.id;
        }
        return parsed;
    }

    constructor(
        action: string,
        sender?: string,
        msg?: string,
        group?: string,
        destination?: string,
        id?: number,
        reason?: string) {
        this.sender = sender;
        this.action = action;
        this.msg = msg;
        this.group = group;
        this.destination = destination;
        this.id = id;
        this.reason = reason;
    }

    public getId(): number {
        return this.id;
    }

    public setId(value: number) {
        this.id = value;
    }

    public getSender(): string {
        return this.sender;
    }

    public setSender(value: string) {
        this.sender = value;
    }

    public getAction(): string {
        return this.action;
    }

    public setAction(value: string) {
        this.action = value;
    }

    public getMessage(): string {
        return this.msg;
    }

    public setMessage(value: string) {
        this.msg = value;
    }

    public getGroup(): string {
        return this.group;
    }

    public setGroup(value: string) {
        this.group = value;
    }

    public getDestination(): string {
        return this.destination;
    }

    public setDestination(value: string) {
        this.destination = value;
    }

    public getReason(): string {
        return this.reason;
    }

    public setReason(value: string) {
        this.reason = value;
    }

    public toJson(): string {
        return JSON.stringify(this);
    }

    public clone(): Message {
        let cloned = new Message(this.action);
        cloned.sender = this.sender;
        cloned.msg = this.msg;
        cloned.group = this.group;
        cloned.destination = this.destination;
        cloned.reason = this.reason;
        cloned.id = this.id;
        return cloned;
    }
}