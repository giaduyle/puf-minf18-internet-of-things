export default class User {
    private readonly id: string;
    private password: string;

    public static fromJson(json: any): User {
        if (json.id == undefined) {
            throw new Error("Illegal argument: the given json is malformed or not containing 'id' field: " + json);
        }
        return new User(json.id, json.password);
    }

    constructor(id: string, pwd?: string) {
        this.id = id;
        this.password = pwd;
    }

    public getId(): string {
        return this.id;
    }

    public getPassword(): string {
        return this.password;
    }

    public setPassword(value: string) {
        this.password = value;
    }

    public toJson(): string {
        return JSON.stringify(this);
    }
}