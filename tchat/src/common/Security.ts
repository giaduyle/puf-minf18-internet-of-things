import {compareSync, hashSync} from "bcrypt";
import User from "./User";
import {ECDH} from "crypto";

export default class Security {
    private static readonly SALT_ROUNDS = 10;
    private static readonly SYMMETRIC_ALGORITHM = "aes-256-ctr";
    private static readonly DEFAULT_CURVE = "secp256k1";
    private static readonly CRYPTO = require('crypto');

    public static readonly DEFAULT_INPUT_CODING = "utf8";
    public static readonly DEFAULT_OUTPUT_CODING = "hex";

    public static encryptPassword(user: User) {
        user.setPassword(Security.hashString(user.getPassword()));
    }

    public static hashString(str: String): string {
        return hashSync(str, Security.SALT_ROUNDS);
    }

    public static compareHashes(source: string, encrypted: string) {
        return compareSync(source, encrypted);
    }

    public static createECDHChannel(): ECDH {
        return Security.CRYPTO.createECDH(Security.DEFAULT_CURVE);
    }

    public static encrypt(plainText: string, secret: Buffer | string): string {
        let iv = Security.CRYPTO.randomBytes(16);
        let cipher = Security.CRYPTO.createCipheriv(Security.SYMMETRIC_ALGORITHM, secret, iv);
        let encrypted = cipher.update(plainText, Security.DEFAULT_INPUT_CODING, Security.DEFAULT_OUTPUT_CODING);
        encrypted += cipher.final(Security.DEFAULT_OUTPUT_CODING);
        return iv.toString(Security.DEFAULT_OUTPUT_CODING) + ":" + encrypted;
    }

    public static decrypt(encrypted: string, secret: Buffer | string): string {
        let encryptedArray = encrypted.split(":");
        let iv = new Buffer(encryptedArray[0], Security.DEFAULT_OUTPUT_CODING);
        let encryptedContent = new Buffer(encryptedArray[1], Security.DEFAULT_OUTPUT_CODING);
        let decipher = Security.CRYPTO.createDecipheriv(Security.SYMMETRIC_ALGORITHM, secret, iv);
        let decrypted = decipher.update(encryptedContent, Security.DEFAULT_OUTPUT_CODING, Security.DEFAULT_INPUT_CODING);
        decrypted += decipher.final(Security.DEFAULT_INPUT_CODING);
        return decrypted;
    }
}