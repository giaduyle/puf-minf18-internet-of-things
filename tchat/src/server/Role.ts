const enum Role {
    OWNER = 3,
    ADMIN = 2,
    MEMBER = 1
}
export {Role};