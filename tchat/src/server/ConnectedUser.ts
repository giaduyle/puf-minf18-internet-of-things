import User from "../common/User";
import {Socket} from "socket.io";

export default class ConnectedUser extends User {
    private readonly secret: Buffer | string;
    private readonly socket: Socket;
    private readonly user: User;

    constructor(user: User, socket: Socket, secret?: Buffer | string) {
        super(null);
        this.user = user;
        this.socket = socket;
        this.secret = secret;
    }

    getUser(): User {
        return this.user;
    }

    getSocket(): Socket {
        return this.socket;
    }

    getId(): string {
        return this.user.getId();
    }

    getSecret(): Buffer | string {
        return this.secret;
    }
}