import Group from "../Group";
import User from "../../common/User";
import Message from "../../common/Message";
import {TCHAT_SCHEMA_SQL} from "./TChatSchema";
import {Database, OPEN_CREATE, OPEN_READWRITE, verbose} from "sqlite3";

export default class DatabaseService {
    private readonly db: Database;

    constructor(callback: (databaseService: DatabaseService, error: Error | null) => void, file?: string) {
        verbose();
        if (file != undefined) {
            let existedDb = require('fs').existsSync(file);
            this.db = new Database(file, OPEN_READWRITE | OPEN_CREATE);
            if (!existedDb) {
                this.reSchema(callback);
            } else {
                callback(this, undefined);
            }
        } else {
            this.db = new Database(':memory:');
            this.reSchema(callback);
        }
    }

    public reSchema(callback: (databaseService: DatabaseService, error: Error | null) => void) {
        this.db.exec(TCHAT_SCHEMA_SQL, (error) => {
            callback(this, error);
        });
    }

    public getUser(userId: string): Promise<User> {
        return new Promise((resolve) => {
            this.db.get("SELECT user_id, pwd FROM users WHERE user_id = ?",
                userId,
                (error, row) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    if (row == undefined) {
                        resolve(undefined);
                        return;
                    }
                    let user = new User(row.user_id);
                    user.setPassword(row.pwd);
                    resolve(user);
                }
            );
        });
    }

    public upsertUser(user: User): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            this.db.run("INSERT OR REPLACE INTO users (user_id, pwd) " +
                "VALUES (?, ?)" +
                ";",
                [
                    user.getId(),
                    user.getPassword()
                ],
                (error) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    resolve(true);
                }
            );
        });
    }

    public getGroup(groupId: string): Promise<Group> {
        return new Promise<Group>((resolve) => {
            let group: Group = undefined;
            this.db.all(
                "SELECT " +
                "g.group_id, g.is_private, " +
                "u.user_id, ug.role, ug.is_ban " +
                "FROM groups AS g " +
                "LEFT JOIN user_groups AS ug ON ug.group_id = g.group_id " +
                "LEFT JOIN users AS u ON ug.user_id = u.user_id " +
                "WHERE ug.group_id = ?" +
                ";",
                groupId,
                (error, rows) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    if (rows == undefined) {
                        resolve(undefined);
                        return;
                    }
                    rows.forEach(row => {
                        if (group == undefined) {
                            group = new Group(row.group_id, row.is_private);
                        }
                        group.putUser(row.user_id, row.role);
                        if (row.is_ban) {
                            group.banUser(row.user_id);
                        }
                    });
                    resolve(group);
                }
            );
        });
    }

    public getAllGroups(): Promise<Map<string, Group>> {
        return new Promise<Map<string, Group>>((resolve) => {
            this.db.all(
                "SELECT " +
                "g.group_id, g.is_private, " +
                "u.user_id, ug.role, ug.is_ban " +
                "FROM groups AS g " +
                "LEFT JOIN user_groups AS ug ON ug.group_id = g.group_id " +
                "LEFT JOIN users AS u ON ug.user_id = u.user_id " +
                ";",
                (error, rows) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    let groups: Map<string, Group> = new Map<string, Group>();
                    if (rows == undefined) {
                        resolve(groups);
                        return;
                    }
                    rows.forEach(row => {
                        if (row != undefined) {
                            let group = groups.get(row.group_id);
                            if (group == undefined) {
                                group = new Group(row.group_id, row.is_private);
                            }
                            if (row.user_id != undefined) {
                                group.putUser(row.user_id, row.role);
                                if (row.is_ban) {
                                    group.banUser(row.user_id);
                                }
                            }
                            groups.set(group.getId(), group);
                        }
                    });
                    resolve(groups);
                }
            );
        });
    }

    public upsertGroup(group: Group): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            this.db.run("INSERT OR REPLACE INTO groups (group_id, is_private) " +
                "VALUES (?, ?)" +
                ";",
                [
                    group.getId(),
                    group.isPrivate() ? 1 : 0
                ],
                (error) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                }
            );
            this.db.run("DELETE FROM user_groups WHERE group_id = ?" +
                ";",
                group.getId(),
                (error) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    group.getUserIds().forEach(userId => {
                        this.db.run("INSERT OR REPLACE INTO user_groups (user_id, group_id, role, is_ban) " +
                            "VALUES (?, ?, ?, ?)" +
                            ";",
                            [
                                userId,
                                group.getId(),
                                group.getUserRole(userId).valueOf(),
                                group.isUserBanned(userId) ? 1: 0
                            ],
                            (error) => {
                                if (error != undefined) {
                                    console.log(error);
                                    return;
                                }
                                resolve(true);
                            }
                        );
                    });
                }
            );
        });
    }

    public getStatesOfGroup(groupId: string): Promise<Message[]> {
        return new Promise<Message[]>((resolve) => {
            this.db.all(
                "SELECT m.action, m.user_id, m.msg, m.destination, m.group_id, m.reason " +
                "FROM messages AS m " +
                "WHERE m.group_id = ?" +
                ";",
                groupId,
                (error, rows) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    if (rows == undefined) {
                        resolve(undefined);
                        return;
                    }
                    let states: Message[] = [];
                    rows.forEach(row => {
                        states.push(new Message(
                            row.action,
                            row.user_id,
                            row.msg,
                            row.group_id,
                            row.destination,
                            row.id,
                            row.reason
                        ));
                    });
                    resolve(states);
                }
            );
        });
    }

    public getAllGroupsStates(): Promise<Map<string, Message[]>> {
        return new Promise<Map<string, Message[]>>((resolve) => {
            this.db.all(
                "SELECT m.action, m.user_id, m.msg, m.destination, m.group_id, m.reason " +
                "FROM messages AS m " +
                ";",
                (error, rows) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    let returnStates: Map<string, Message[]> = new Map<string, Message[]>();
                    if (rows == undefined) {
                        resolve(returnStates);
                        return;
                    }
                    rows.forEach(row => {
                        let groupStates = returnStates.get(row.group_id);
                        if (groupStates == undefined) {
                            groupStates = [];
                            returnStates.set(row.group_id, groupStates);
                        }
                        groupStates.push(new Message(
                            row.action,
                            row.user_id,
                            row.msg,
                            row.group_id,
                            row.destination,
                            row.id,
                            row.reason
                        ));
                    });
                    resolve(returnStates);
                },
            );
        });
    }

    public getMessagesOfUser(userId: string): Promise<Message[]> {
        return new Promise<Message[]>((resolve) => {
            this.db.all(
                "SELECT m.action, m.user_id, m.msg, m.destination, m.group_id, m.reason " +
                "FROM messages AS m " +
                "WHERE (m.user_id = ? OR m.destination = ?) AND m.msg IS NOT NULL" +
                ";",
                userId,
                (error, rows) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    let states: Message[] = [];
                    if (rows == undefined) {
                        resolve(states);
                        return;
                    }
                    rows.forEach(row => {
                        let state = new Message(
                            row.action,
                            row.user_id,
                            row.msg,
                            row.group_id,
                            row.destination,
                            row.id,
                            row.reason
                        );
                        states.push(state);
                    });
                    resolve(states);
                }
            );

        });
    }

    public upsertMessage(message: Message): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            this.db.run(
                "INSERT OR REPLACE INTO messages (action, user_id, msg, group_id, destination, reason) " +
                " VALUES (?, ?, ?, ?, ?, ?)" +
                ";",
                [
                    message.getAction(),
                    message.getSender(),
                    message.getMessage(),
                    message.getGroup(),
                    message.getDestination(),
                    message.getReason()
                ],
                (error) => {
                    if (error != undefined) {
                        console.log(error);
                        return;
                    }
                    resolve(true);
                }
            );
        });
    }
}