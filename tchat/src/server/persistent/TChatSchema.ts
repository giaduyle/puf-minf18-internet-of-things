export const TCHAT_SCHEMA_SQL =
    `
        PRAGMA foreign_keys = ON;
        BEGIN TRANSACTION;
        DROP TABLE IF EXISTS "users";
        CREATE TABLE IF NOT EXISTS "users" (
            "user_id"       TEXT NOT NULL PRIMARY KEY UNIQUE,
            "pwd"           TEXT NOT NULL
        );
        DROP TABLE IF EXISTS "groups";
        CREATE TABLE IF NOT EXISTS "groups" (
            "group_id"      TEXT NOT NULL PRIMARY KEY UNIQUE,
            "is_private"    INTEGER
        );
        DROP TABLE IF EXISTS "user_groups";
        CREATE TABLE IF NOT EXISTS "user_groups" (
            "user_id"       TEXT NOT NULL,
            "group_id"      TEXT NOT NULL,
            "role"          INTEGER NOT NULL DEFAULT 1,
            "is_ban"        INTEGER NOT NULL DEFAULT 0,
            FOREIGN KEY(user_id) references users("user_id"),
            FOREIGN KEY(group_id) references "groups"("group_id")
        );
        DROP TABLE IF EXISTS "messages";
        CREATE TABLE IF NOT EXISTS "messages" (
            "message_id"    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
            "user_id"       TEXT NOT NULL,
            "action"        TEXT NOT NULL,
            "msg"           TEXT,
            "group_id"      TEXT,
            "destination"   TEXT,
            "reason"        TEXT,
            FOREIGN KEY(user_id) references users("user_id"),
            FOREIGN KEY(group_id) references "groups"("group_id")
        );
        COMMIT;
    `;