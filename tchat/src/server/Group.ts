import {Role} from "./Role";

export default class Group {
    private readonly id: string;
    private readonly privateGroup: boolean = true;
    private users: Map<string, Role> = new Map<string, Role>();
    private bans: Set<string> = new Set<string>();

    constructor(id: string, isPrivate?: boolean) {
        this.id = id;
        if (isPrivate != undefined) {
            this.privateGroup = isPrivate;
        }
    }

    public getUserIds(): string[] {
        return Array.from(this.users.keys());
    }

    public getBannedUserIds(): string[] {
        return Array.from(this.bans.values());
    }

    public getId(): string {
        return this.id;
    }

    public getUserRole(userId: string): Role {
        return this.users.get(userId);
    }

    public isPrivate(): boolean {
        return this.privateGroup;
    }

    public hasUser(userId: string): boolean {
        return this.users.has(userId);
    }

    public putUser(userId: string, role: Role): void {
        this.users.set(userId, role);
    }

    public removeUser(userId: string): boolean {
        return this.users.delete(userId);
    }

    public banUser(userId: string): void {
        this.bans.add(userId);
    }

    public unbanUser(userId: string): void {
        this.bans.delete(userId);
    }

    public isUserBanned(userId: string): boolean {
        return this.bans.has(userId);
    }
}