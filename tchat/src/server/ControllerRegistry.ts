import MessageController from "./MessageController";
import BroadcastAllController from "./controllers/BroadcastAllController";
import SendPrivatelyController from "./controllers/SendPrivatelyController";
import ListUsersController from "./controllers/ListUsersController";
import QuitController from "./controllers/QuitController";
import CreateGroupController from "./controllers/CreateGroupController";
import BanFromGroupController from "./controllers/BanFromGroupController";
import BroadcastGroupController from "./controllers/BroadcastGroupController";
import InviteToGroupController from "./controllers/InviteToGroupController";
import JoinGroupController from "./controllers/JoinGroupController";
import KickFromGroupController from "./controllers/KickFromGroupController";
import LeaveGroupController from "./controllers/LeaveGroupController";
import ListGroupsController from "./controllers/ListGroupsController";
import ListMessagesController from "./controllers/ListMessagesController";
import ListGroupMembersController from "./controllers/ListGroupMembersController";
import UnbanFromGroupController from "./controllers/UnbanFromGroupController";
import ListGroupStatesController from "./controllers/ListGroupStatesController";
import AddGroupAdminController from "./controllers/AddGroupAdminController";
import RemoveGroupAdminController from "./controllers/RemoveGroupAdminController";
import ChangePasswordController from "./controllers/ChangePasswordController";
import ListGroupBansController from "./controllers/ListGroupBansController";

export default class ControllerRegistry {
    public static readonly DEFAULT_CONTROLLERS: MessageController[] = [
        new AddGroupAdminController(),
        new BanFromGroupController(),
        new BroadcastAllController(),
        new BroadcastGroupController(),
        new ChangePasswordController(),
        new CreateGroupController(),
        new InviteToGroupController(),
        new JoinGroupController(),
        new KickFromGroupController(),
        new LeaveGroupController(),
        new ListGroupBansController(),
        new ListGroupMembersController(),
        new ListGroupsController(),
        new ListGroupStatesController(),
        new ListMessagesController(),
        new ListUsersController(),
        new QuitController(),
        new RemoveGroupAdminController(),
        new SendPrivatelyController(),
        new UnbanFromGroupController(),
    ];

    private controllers: Map<string, MessageController> = new Map<string, MessageController>();

    public static readonly DEFAULT_INSTANCE: ControllerRegistry =
        new ControllerRegistry(ControllerRegistry.DEFAULT_CONTROLLERS);

    constructor(controllers: MessageController[]) {
        controllers.forEach(controller => {
            if (this.controllers.has(controller.getControllingAction())) {
                throw Error(`Duplicated controller action: ${controller.getControllingAction()}`)
            }
            this.controllers.set(controller.getControllingAction(), controller);
        });
    }

    public getController(forAction: string): MessageController | undefined {
        return this.controllers.get(forAction);
    }
}