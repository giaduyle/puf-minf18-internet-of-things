import Server from "./Server";
import User from "../common/User";
import Message from "../common/Message";

export default class Respond {
    static ok(server: Server, toUser: User, message: string) {
        let okMessage = new Message("ok");
        okMessage.setMessage(message);
        Respond.respond(server, toUser, okMessage);
    }

    static error(server: Server, toUser: User, message: string) {
        let okMessage = new Message("error");
        okMessage.setMessage(message);
        Respond.respond(server, toUser, okMessage);
    }

    static warn(server: Server, toUser: User, message: string) {
        let okMessage = new Message("warning");
        okMessage.setMessage(message);
        Respond.respond(server, toUser, okMessage);
    }

    private static respond(server: Server, user: User, message: Message): void {
        message.setSender("Server");
        message.setDestination(user.getId());
        server.getUserService().sendToUser(user.getId(), message);
    }
}