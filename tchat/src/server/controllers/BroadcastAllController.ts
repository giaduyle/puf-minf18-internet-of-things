import MessageController from "../MessageController";
import Message from "../../common/Message";
import Server from "../Server";
import User from "../../common/User";

export default class BroadcastAllController implements MessageController {
    handle(server: Server, user: User, message: Message): void {
        server.broadcast(message);
    }

    getControllingAction(): string {
        return "broadcast";
    }
}