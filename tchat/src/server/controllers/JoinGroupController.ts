import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";
import Notifier from "../Notifier";

export default class JoinGroupController implements MessageController {
    getControllingAction(): string {
        return "join";
    }

    handle(server: Server, sender: User, message: Message): void {
        let groupManager = server.getGroupService();
        let groupId = message.getGroup();
        let senderId = sender.getId();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender,
                    "Group '" + groupId + "' is not found!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from group ${groupId}`);
                return;
            }
            if (group.hasUser(senderId)) {
                Respond.warn(server, sender,
                    "User '" + senderId + "' is already in group '" + message.getGroup() + "'!");
                return;
            }
            groupManager.addUserToGroup(senderId, message.getGroup());
            server.getGroupService().putState(message);
            Notifier.notifyGroup(server, group, `User ${senderId} joined group!`);
        });
    }
}