import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";
import {Role} from "../Role";
import Notifier from "../Notifier";

export default class BanFromGroupController implements MessageController {
    getControllingAction(): string {
        return "ban";
    }

    handle(server: Server, sender: User, message: Message): void {
        let senderId = sender.getId();
        let targetUserId = message.getDestination();
        let groupManager = server.getGroupService();
        let groupId = message.getGroup();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender,
                    "Group '" + groupId + "' is not found!");
                return;
            }
            if (!group.hasUser(senderId)) {
                Respond.error(server, sender,
                    "You do not belong to the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from the group ${groupId}!`);
                return;
            }
            if (!group.hasUser(targetUserId)) {
                Respond.error(server, sender,
                    "User: " + targetUserId + " is not in group: " + groupId + "!");
                return;
            }
            if (group.getUserRole(senderId) < Role.ADMIN) {
                Respond.error(server, sender,
                    "You do not have enough privileges to ban that user!");
                return;
            }
            if (sender.getId() == targetUserId) {
                Respond.error(server, sender, "You cannot ban yourself!");
                return;
            }
            if (group.isUserBanned(targetUserId)) {
                Respond.error(server, sender,
                    `User ${targetUserId} is already banned from the group ${groupId}!`);
                return;
            }
            if (group.getUserRole(targetUserId) > group.getUserRole(senderId)) {
                Respond.error(server, sender,
                    `User ${targetUserId} has higher role than you!`);
                return;
            }
            group.banUser(targetUserId);
            groupManager.attemptSaveGroupToDatabase(group).then(() => {
                groupManager.putState(message);
                Notifier.notifyGroup(server, group, `User ${targetUserId} is banned from group!`);
            });
        });
    }
}