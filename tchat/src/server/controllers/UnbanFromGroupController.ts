import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";
import {Role} from "../Role";

export default class UnbanFromGroupController implements MessageController {
    getControllingAction(): string {
        return "unban";
    }

    handle(server: Server, sender: User, message: Message): void {
        let senderId = sender.getId();
        let targetUserId = message.getDestination();
        let groupId = message.getGroup();
        let groupManager = server.getGroupService();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender,
                    "Group '" + groupId + "' is not found!");
                return;
            }
            if (!group.hasUser(senderId)) {
                Respond.error(server, sender,
                    "You do not belong to the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from the group ${groupId}!`);
                return;
            }
            if (!group.hasUser(targetUserId)) {
                Respond.error(server, sender,
                    "User: " + targetUserId + " is not in group: " + groupId + "!");
                return;
            }
            if (sender.getId() == targetUserId) {
                Respond.error(server, sender, "You cannot un-ban yourself!");
                return;
            }
            if (group.getUserRole(senderId) < Role.ADMIN) {
                Respond.error(server, sender,
                    "You do not have enough privileges to un-ban an user!");
                return;
            }
            group.unbanUser(targetUserId);
            groupManager.attemptSaveGroupToDatabase(group).then(() => {
                groupManager.putState(message);
                Respond.ok(server, sender,
                    "User '" + targetUserId + "' is unbanned from group '" + groupId + "'!");
            });
        });

    }
}