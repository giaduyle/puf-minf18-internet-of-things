import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";
import {prettifyMessagesToLines} from "../../common/Utils";

export default class ListMessagesController implements MessageController {
    getControllingAction(): string {
        return "messages";
    }

    handle(server: Server, sender: User, message: Message): void {
        let senderId = sender.getId();
        let groupId = message.getGroup();
        let userManager = server.getUserService();
        if (groupId == undefined || groupId.length == 0) {
            userManager.getMessages(sender.getId()).then(messages => {
                Respond.ok(server, sender, "Messages: \n" + prettifyMessagesToLines(messages));
                return;
            });
        }
        let groupManager = server.getGroupService();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender,
                    "Unknown group: " + groupId + "!");
                return;
            }
            if (!group.hasUser(senderId)) {
                Respond.error(server, sender,
                    "You do not belong to the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from the group ${groupId}!`);
                return;
            }
            groupManager.getMessages(groupId).then(messages => {
                Respond.ok(server, sender, "Messages: \n" + prettifyMessagesToLines(messages));
            });
        });
    }
}