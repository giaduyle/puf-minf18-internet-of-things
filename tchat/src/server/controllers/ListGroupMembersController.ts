import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";

export default class ListGroupMembersController implements MessageController {
    getControllingAction(): string {
        return "members";
    }

    handle(server: Server, sender: User, message: Message): void {
        let groupId = message.getGroup();
        let senderId = sender.getId();
        let groupManager = server.getGroupService();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender,
                    "Unknown group: " + groupId + "!");
                return;
            }
            if (!group.hasUser(senderId)) {
                Respond.error(server, sender,
                    "You do not belong to the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from the group ${groupId}!`);
                return;
            }
            let userIds: string[] = group.getUserIds();
            Respond.ok(server, sender, "Users: \n" + userIds.join("\n"));
        });
    }
}