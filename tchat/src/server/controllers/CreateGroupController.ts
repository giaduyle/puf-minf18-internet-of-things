import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Group from "../Group";
import Respond from "../Respond";
import {Role} from "../Role";

export default class CreateGroupController implements MessageController {
    getControllingAction(): string {
        return "create group";
    }

    handle(server: Server, sender: User, message: Message) {
        let groupId = message.getGroup();
        let groupManager = server.getGroupService();
        groupManager.getGroup(groupId).then(group => {
            if (group != undefined) {
                Respond.error(server, sender, "Group '" + groupId + "' is already existed!");
                return;
            }
            let newGroup = new Group(groupId, true);
            newGroup.putUser(sender.getId(), Role.OWNER);
            groupManager.putGroup(newGroup).then(() => {
                Respond.ok(server, sender, "Group '" + groupId + "' created successfully!");
            });
            groupManager.putState(message);
        });
    }
}