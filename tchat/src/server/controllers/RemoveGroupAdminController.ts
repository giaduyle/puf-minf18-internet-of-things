import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";
import {Role} from "../Role";
import Notifier from "../Notifier";

export default class RemoveGroupAdminController implements MessageController {
    getControllingAction(): string {
        return "remove admin";
    }

    handle(server: Server, sender: User, message: Message): void {
        let senderId = sender.getId();
        let targetUserId = message.getDestination();
        let groupId = message.getGroup();
        let groupManager = server.getGroupService();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender, "Unknown group: " + groupId + "!");
                return;
            }
            if (!group.hasUser(senderId)) {
                Respond.error(server, sender,
                    "You do not belong to the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from the group ${groupId}!`);
                return;
            }
            if (group.isUserBanned(targetUserId)) {
                Respond.error(server, sender,
                    `User ${targetUserId} is banned from the group ${groupId}!`);
                return;
            }
            if (group.getUserRole(senderId) < Role.ADMIN) {
                Respond.error(server, sender,
                    "You do not have enough privileges to remove an admin!");
                return;
            }
            if (group.getUserRole(targetUserId) > group.getUserRole(senderId)) {
                Respond.error(server, sender,
                    `User ${targetUserId} has higher role than you!`);
                return;
            }
            if (sender.getId() == targetUserId) {
                Respond.error(server, sender, "You cannot remove your own admin role!");
                return;
            }
            group.putUser(targetUserId, Role.MEMBER);
            groupManager.attemptSaveGroupToDatabase(group).then(() => {
                groupManager.putState(message);
                Notifier.notifyGroup(server, group,
                    "Removed admin role of user '" + targetUserId + "' in group '" + groupId + "'!");
            });
        });
    }
}