import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";
import {Role} from "../Role";
import Notifier from "../Notifier";

export default class AddGroupAdminController implements MessageController {
    getControllingAction(): string {
        return "add admin";
    }

    handle(server: Server, sender: User, message: Message): void {
        let senderId = sender.getId();
        let groupId = message.getGroup();
        let groupManager = server.getGroupService();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender, "Unknown group: " + groupId + "!");
                return;
            }
            if (!group.hasUser(senderId)) {
                Respond.error(server, sender,
                    "You do not belong to the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from the group ${groupId}!`);
                return;
            }
            if (group.getUserRole(senderId) < Role.ADMIN) {
                Respond.error(server, sender,
                    "You do not have enough privileges to add an admin!");
                return;
            }
            let targetUserId = message.getDestination();
            if (senderId == targetUserId) {
                Respond.error(server, sender,
                    "You are already the admin!");
                return;
            }
            if (!group.hasUser(targetUserId)) {
                Respond.error(server, sender,
                    "User: " + targetUserId + " is not the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(targetUserId)) {
                Respond.error(server, sender,
                    `User ${targetUserId} is banned from the group ${groupId}!`);
                return;
            }
            if (group.getUserRole(targetUserId) >= Role.ADMIN) {
                Respond.error(server, sender,
                    "User '" + targetUserId + "' is already an admin or higher in group '" + groupId + "'!");
                return;
            }
            if (sender.getId() == targetUserId) {
                Respond.error(server, sender, "You cannot add yourself as admin!");
                return;
            }
            group.putUser(targetUserId, Role.ADMIN);
            groupManager.attemptSaveGroupToDatabase(group).then(() => {
                server.getGroupService().putState(message);
                Notifier.notifyGroup(server, group,
                    "Granted user '" + targetUserId + "' admin role in group '" + groupId + "'!");
            });
        });
    }
}