import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";

export default class ChangePasswordController implements MessageController {
    getControllingAction(): string {
        return "change password";
    }

    handle(server: Server, sender: User, message: Message): void {
        let newPassword = message.getMessage();
        sender.setPassword(newPassword);
        server.getUserService().attemptSaveUserToDatabase(sender).then(() => {
            Respond.ok(server, sender, "Password changed successfully!");
        });
    }
}
