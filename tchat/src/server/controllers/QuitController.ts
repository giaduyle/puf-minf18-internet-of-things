import MessageController from "../MessageController";
import Server from "../Server";
import Message from "../../common/Message";
import User from "../../common/User";

export default class QuitController implements MessageController {
    handle(server: Server, user: User, message: Message): void {
        let userId = user.getId();
        if (server.getUserService().disconnectUser(userId)) {
            console.log(userId + " left chat!");
        }
    }

    getControllingAction(): string {
        return "quit";
    }
}