import MessageController from "../MessageController";
import Server from "../Server";
import Message from "../../common/Message";
import User from "../../common/User";
import Respond from "../Respond";

export default class ListUsersController implements MessageController {
    getControllingAction(): string {
        return "list";
    }

    handle(server: Server, user: User, message: Message): void {
        let userManager = server.getUserService();
        Respond.ok(server, user, "Connected users: \n" + userManager.getConnectedUserIds().join("\n"));
    }
}