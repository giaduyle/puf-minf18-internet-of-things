import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";

export default class ListGroupsController implements MessageController {
    getControllingAction(): string {
        return "groups";
    }

    handle(server: Server, sender: User, message: Message): void {
        let groupManager = server.getGroupService();
        Respond.ok(server, sender, "Existing groups: \n" + groupManager.getGroupIds().join("\n"));
    }
}