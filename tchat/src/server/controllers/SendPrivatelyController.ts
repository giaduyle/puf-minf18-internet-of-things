import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";

export default class SendPrivatelyController implements MessageController {
    handle(server: Server, sender: User, message: Message): void {
        let receiverId = message.getDestination();
        let userManager = server.getUserService();
        if (!userManager.isUserConnected(receiverId)) {
            Respond.error(server, sender, "User is offline: " + receiverId);
            return;
        }
        userManager.sendToUser(receiverId, message);
        server.getDatabaseService().upsertMessage(message);
    }

    getControllingAction(): string {
        return "send";
    }
}