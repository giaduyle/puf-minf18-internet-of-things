import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";
import Notifier from "../Notifier";

export default class InviteToGroupController implements MessageController {
    getControllingAction(): string {
        return "invite";
    }

    handle(server: Server, sender: User, message: Message): void {
        let senderId = sender.getId();
        let groupId = message.getGroup();
        let groupManager = server.getGroupService();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender,
                    "Group '" + groupId + "' is not found!");
                return;
            }
            if (!group.hasUser(sender.getId())) {
                Respond.error(server, sender,
                    "You do not belong to the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from the group ${groupId}!`);
                return;
            }
            let targetUserId = message.getDestination();
            if (group.isUserBanned(targetUserId)) {
                Respond.error(server, sender,
                    `User ${targetUserId} is banned from the group ${groupId}!`);
                return;
            }
            if (group.hasUser(targetUserId)) {
                Respond.warn(server, sender,
                    "User '" + targetUserId + "' is already in group '" + message.getGroup() + "'!");
                return;
            }
            server.getDatabaseService().getUser(targetUserId).then(targetUser => {
                if (targetUser == null) {
                    Respond.error(server, sender, `User ${targetUserId} is not existed`);
                    return;
                }
                groupManager.addUserToGroup(targetUserId, groupId);
                server.getGroupService().putState(message);
                Notifier.notifyGroup(server, group, `User ${targetUserId} is invited to group!`);
            });
        });
    }
}