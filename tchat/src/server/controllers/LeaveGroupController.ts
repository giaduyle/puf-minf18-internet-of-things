import MessageController from "../MessageController";
import Server from "../Server";
import User from "../../common/User";
import Message from "../../common/Message";
import Respond from "../Respond";
import Notifier from "../Notifier";

export default class LeaveGroupController implements MessageController {
    getControllingAction(): string {
        return "leave";
    }

    handle(server: Server, sender: User, message: Message): void {
        let senderId = sender.getId();
        let groupId = message.getGroup();
        let groupManager = server.getGroupService();
        groupManager.getGroup(groupId).then(group => {
            if (group == undefined) {
                Respond.error(server, sender,
                    "Unknown group: " + groupId + "!");
                return;
            }
            if (!group.hasUser(senderId)) {
                Respond.error(server, sender,
                    "You do not belong to the group: " + groupId + "!");
                return;
            }
            if (group.isUserBanned(senderId)) {
                Respond.error(server, sender,
                    `You are banned from the group ${groupId}!`);
                return;
            }
            groupManager.removeUserFromGroup(senderId, groupId);
            server.getGroupService().putState(message);
            Notifier.notifyGroup(server, group, `User ${senderId} left group!`);
        });
    }
}