import Group from "../Group";
import DatabaseService from "../persistent/DatabaseService";
import Message from "../../common/Message";
import {Role} from "../Role";

export default class GroupService {
    private groups: Map<string, Group> = new Map<string, Group>();
    private groupsStates: Map<string, Message[]> = new Map<string, Message[]>();

    private readonly dbManager: DatabaseService;

    constructor(dbManager?: DatabaseService) {
        if (dbManager != null) {
            this.dbManager = dbManager;
            this.dbManager.getAllGroups().then(groups => {
                this.groups = groups;
            });
            this.dbManager.getAllGroupsStates().then(states => {
                this.groupsStates = states;
            });
        }
    }

    public getGroup(groupId: string): Promise<Group> {
        let group = this.groups.get(groupId);
        if (group != undefined) {
            return new Promise<Group>((resolve) => resolve(this.groups.get(groupId)));
        }
        return this.attemptLoadGroupFromDatabase(groupId);
    }

    public getStates(groupId: string): Promise<Message[]> {
        let states = this.groupsStates.get(groupId);
        if (states != undefined) {
            return new Promise<Message[]>((resolve) => resolve(states));
        }
        return this.attemptLoadStatesFromDatabase(groupId);
    }

    public getMessages(groupId: string): Promise<Message[]> {
        return this.getStates(groupId).then(messages => {
            if (messages != undefined) {
                return this.extractOnlyMessages(messages);
            }
        });
    }

    public putGroup(group: Group): Promise<boolean | void> {
        return this.attemptSaveGroupToDatabase(group).then(() => {
            this.groups.set(group.getId(), group);
        });
    }

    public putState(state: Message) {
        this.attemptSaveStateToDatabase(state).then(() => {
            let states = this.groupsStates.get(state.getGroup());
            if (states == undefined) {
                states = [];
                this.groupsStates.set(state.getGroup(), states);
            }
            states.push(state);
        });
    }

    public getGroupIds(): string[] {
        return Array.from(this.groups.keys());
    }

    public addUserToGroup(userId: string, groupId: string) {
        this.addUserToGroupWithRole(userId, groupId, Role.MEMBER);
    }

    public addUserToGroupWithRole(userId: string, groupId: string, role: Role) {
        let group = this.groups.get(groupId);
        group.putUser(userId, role);
        this.attemptSaveGroupToDatabase(group).then(() => {
            console.log(`Added ${userId} to group ${groupId}`)
        });
    }

    public removeUserFromGroup(userId: string, groupId: string) {
        if (!this.groups.has(groupId)) {
            return;
        }
        let group = this.groups.get(groupId);
        group.removeUser(userId);
        this.attemptSaveGroupToDatabase(group).then(() => {
            console.log(`Removed ${userId} from group ${groupId}`)
        });
    }

    private attemptLoadGroupFromDatabase(groupId: string): Promise<Group> {
        return this.dbManager.getGroup(groupId);
    }

    private attemptLoadStatesFromDatabase(groupId: string): Promise<Message[]> {
        return this.dbManager.getStatesOfGroup(groupId).then(foundStates => {
            if (foundStates != undefined) {
                this.groupsStates.set(groupId, foundStates);
            }
            return foundStates;
        });
    }

    private attemptSaveStateToDatabase(message: Message): Promise<boolean> {
        if (this.dbManager == undefined) {
            return new Promise<boolean>((resolve) => resolve(true));
        }
        return this.dbManager.upsertMessage(message);
    }

    public attemptSaveGroupToDatabase(group: Group): Promise<boolean> {
        if (this.dbManager == undefined) {
            return new Promise<boolean>((resolve) => resolve(false));
        }
        return this.dbManager.upsertGroup(group);
    }

    private extractOnlyMessages(states: Message[]): Message[] {
        let onlyMessages: Message[] = [];
        states.forEach(state => {
            if (state.getMessage() != undefined) {
                onlyMessages.push(state);
            }
        });
        return onlyMessages;
    }
}