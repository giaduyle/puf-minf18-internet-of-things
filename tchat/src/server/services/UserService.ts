import ConnectedUser from "../ConnectedUser";
import User from "../../common/User";
import {Socket} from "socket.io";
import DatabaseService from "../persistent/DatabaseService";
import Message from "../../common/Message";
import Security from "../../common/Security";

export default class UserService {
    private readonly connectedUsers: Map<string, ConnectedUser> = new Map<string, ConnectedUser>();
    private readonly dbManager: DatabaseService;

    constructor(dbManager?: DatabaseService) {
        if (dbManager != null) {
            this.dbManager = dbManager;
        }
    }

    public isUserConnected(userId: string): boolean {
        let userSocket = this.connectedUsers.get(userId);
        if (userSocket == undefined) {
            return false;
        }
        return userSocket.getSocket().connected;
    }

    public getMessages(userId: string): Promise<Message[]> {
        return this.attemptLoadStatesFromDatabase(userId);
    }

    public authenticateUser(user: User): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            if (this.dbManager == undefined) {
                let savedUser = this.connectedUsers.get(user.getId());
                this.checkUserAuthenticity(user, savedUser, resolve, reject);
                return;
            }
            this.dbManager.getUser(user.getId()).then(savedUser => {
                this.checkUserAuthenticity(user, savedUser, resolve, reject);
            });
        });
    }

    private checkUserAuthenticity(
            thisUser: User,
            loadedUser: User,
            resolve: (value?: User) => void,
            reject: (reason?: any) => void) {
        if (loadedUser == undefined) {
            this.attemptSaveUserToDatabase(thisUser)
                .then(() => {
                    resolve(thisUser);
                });
            return;
        }
        if (!Security.compareHashes(thisUser.getPassword(), loadedUser.getPassword())) {
            reject("Invalid password!");
            return;
        }
        resolve(loadedUser);
    }

    public putConnectedUser(user: User, socket: Socket, secret?: Buffer | string): void {
        if (this.connectedUsers.has(user.getId())) {
            this.disconnectUser(user.getId());
        }
        this.connectedUsers.set(user.getId(), new ConnectedUser(user, socket, secret));
    }

    public getConnectedUserIds(): string[] {
        return Array.from(this.connectedUsers.values()).map((user: ConnectedUser) => user.getId());
    }

    sendToUser(userId: string, message: Message) {
        if (!this.isUserConnected(userId)) {
            return;
        }
        let encrypted = Security.encrypt(message.toJson(), this.getUserSecret(userId));
        this.getUserSocket(userId).send(encrypted);
    }

    getUserSocket(userId: string): Socket | undefined {
        if (!this.connectedUsers.has(userId)) {
            return undefined;
        }
        return this.connectedUsers.get(userId).getSocket();
    }

    getUserSecret(userId: string): Buffer | string | undefined {
        if (!this.connectedUsers.has(userId)) {
            return undefined;
        }
        return this.connectedUsers.get(userId).getSecret();
    }

    disconnectUser(userId: string): boolean {
        if (!this.isUserConnected(userId)) {
            return false;
        }
        let socket = this.getUserSocket(userId);
        this.connectedUsers.delete(userId);
        socket.disconnect(true);
        return true;
    }

    private attemptLoadStatesFromDatabase(userId: string): Promise<Message[]> {
        if (this.dbManager == undefined) {
            return new Promise((resolve) => resolve([]));
        }
        return this.dbManager.getMessagesOfUser(userId);
    }

    public attemptSaveUserToDatabase(user: User) {
        if (this.dbManager == undefined) {
            return;
        }
        Security.encryptPassword(user);
        return this.dbManager.upsertUser(user);
    }
}