import * as SocketIO from "socket.io";
import Message from "../common/Message";
import User from "../common/User";
import UserService from "./services/UserService";
import ControllerRegistry from "./ControllerRegistry";
import GroupService from "./services/GroupService";
import DatabaseService from "./persistent/DatabaseService";
import Security from "../common/Security";
import {Socket} from "socket.io";
import Respond from "./Respond";

export default class Server {
    private readonly port: number;
    private readonly controllerRegistry: ControllerRegistry;
    private databaseManager: DatabaseService;
    private userManager: UserService;
    private groupManager: GroupService;

    private io: SocketIO.Server;

    constructor(port: number, databaseFile: string = "./TChat.db") {
        this.port = port;
        this.controllerRegistry = ControllerRegistry.DEFAULT_INSTANCE;
        new DatabaseService((databaseService, error) => {
            if (error != undefined) {
                console.log(error);
            }
            this.databaseManager = databaseService;
            this.userManager = new UserService(databaseService);
            this.groupManager = new GroupService(databaseService);
        }, databaseFile);
    }

    public start() {
        this.io = SocketIO.listen(this.port);
        this.authenticateIfUsersConnect();
        console.log("Started on port: " + this.port);
    }

    public broadcast(message: Message): void {
        this.io.sockets.emit("broadcast", message.toJson());
    }

    getUserService(): UserService {
        return this.userManager;
    }

    getGroupService(): GroupService {
        return this.groupManager;
    }

    getDatabaseService(): DatabaseService {
        return this.databaseManager;
    }

    private authenticateIfUsersConnect() {
        this.io.sockets.on('connection', (socket) => {
            let publicKey = socket.handshake.query.publicKey;
            let secureChannel = Security.createECDHChannel();
            let serverPublicKey = secureChannel.generateKeys(Security.DEFAULT_OUTPUT_CODING);
            socket.emit("server-public-key", serverPublicKey);
            let secret = secureChannel.computeSecret(publicKey, Security.DEFAULT_OUTPUT_CODING);
            socket.on("login", (data) => {
                let decrypted = Security.decrypt(data, secret);
                let user: User = User.fromJson(JSON.parse(decrypted));
                this.userManager.putConnectedUser(user, socket, secret);
                this.userManager.authenticateUser(user)
                    .then(user => {
                            console.log('User ' + user.getId() + ' joined chat!');
                            this.listenMessageEvents(user, socket);
                            this.listenDisconnectionEvents(user, socket);
                        }
                    ).catch((error) => {
                        Respond.error(this, user, error);
                        this.userManager.disconnectUser(user.getId());
                    });
            });
        });
    }

    private listenMessageEvents(user: User, socket: Socket) {
        socket.on('message', (data) => {
            let decrypted = Security.decrypt(data, this.userManager.getUserSecret(user.getId()));
            let message: Message = Message.fromJson(JSON.parse(decrypted));
            let action: string = message.getAction();
            let controller = this.controllerRegistry.getController(action);
            if (controller != undefined) {
                controller.handle(this, new User(message.getSender()), message);
            } else {
                Respond.error(this, user, `Unknown action: ${action}`)
            }
        });
    }

    private listenDisconnectionEvents(user: User, socket: Socket) {
        socket.on("disconnect", () => {
            console.log('User ' + user.getId() + ' disconnected!');
        });
    }
}

export const main = async (): Promise<void> => {
    return new Promise<void>(() => {
        new Server(3636).start();
    });
};
main().catch(error => console.log(error));