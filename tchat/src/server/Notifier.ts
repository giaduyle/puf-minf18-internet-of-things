import Server from "./Server";
import Group from "./Group";
import Message from "../common/Message";

export default class Notifier {
    public static notifyGroup(server: Server, group: Group, message: string) {
        let notification = new Message("notification");
        notification.setSender("Server");
        notification.setMessage(message);
        let userService = server.getUserService();
        group.getUserIds().forEach(userId => {
            userService.sendToUser(userId, notification);
        })
    }
}