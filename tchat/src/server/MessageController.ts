import Message from "../common/Message";
import Server from "./Server";
import User from "../common/User";

export default interface MessageController {
    getControllingAction(): string;

    handle(server: Server, sender: User, message: Message): void;
}