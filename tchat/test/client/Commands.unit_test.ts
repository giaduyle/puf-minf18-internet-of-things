import AddGroupAdmin from "../../src/client/commands/AddGroupAdmin";
import Message from "../../src/common/Message";
import BanFromGroup from "../../src/client/commands/BanFromGroup";
import BroadcastAll from "../../src/client/commands/BroadcastAll";
import BroadcastGroup from "../../src/client/commands/BroadcastGroup";
import ChangePassword from "../../src/client/commands/ChangePassword";
import CreateGroup from "../../src/client/commands/CreateGroup";
import Help from "../../src/client/commands/Help";
import InviteToGroup from "../../src/client/commands/InviteToGroup";
import JoinGroup from "../../src/client/commands/JoinGroup";
import KickFromGroup from "../../src/client/commands/KickFromGroup";
import LeaveGroup from "../../src/client/commands/LeaveGroup";
import ListGroupBans from "../../src/client/commands/ListGroupBans";
import ListGroupMembers from "../../src/client/commands/ListGroupMembers";
import ListGroups from "../../src/client/commands/ListGroups";
import ListGroupStates from "../../src/client/commands/ListGroupStates";
import ListMessages from "../../src/client/commands/ListMessages";
import ListUsers from "../../src/client/commands/ListUsers";
import Quit from "../../src/client/commands/Quit";
import RemoveGroupAdmin from "../../src/client/commands/RemoveGroupAdmin";
import SendPrivately from "../../src/client/commands/SendPrivately";
import UnbanFromGroup from "../../src/client/commands/UnbanFromGroup";

let expect = require('chai').expect;

describe('Commands', () => {
    it('AddGroupAdmin', () => {
        // GIVEN
        let cmd = new AddGroupAdmin();
        cmd.setArgs(["group", "user"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expectedMessage.setDestination("user");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('BanFromGroup', () => {
        // GIVEN
        let cmd = new BanFromGroup();
        cmd.setArgs(["group", "user", "reason"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expectedMessage.setDestination("user");
        expectedMessage.setReason("reason");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('BroadcastAll', () => {
        // GIVEN
        let cmd = new BroadcastAll();
        cmd.setArgs(["message"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setMessage("message");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('BroadcastGroup', () => {
        // GIVEN
        let cmd = new BroadcastGroup();
        cmd.setArgs(["group", "message"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expectedMessage.setMessage("message");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('ChangePassword', () => {
        // GIVEN
        let cmd = new ChangePassword();
        cmd.setArgs(["password"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setMessage("password");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('CreateGroup', () => {
        // GIVEN
        let cmd = new CreateGroup();
        cmd.setArgs(["group"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('Help', () => {
        // GIVEN
        let cmd = new Help();

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('InviteToGroup', () => {
        // GIVEN
        let cmd = new InviteToGroup();
        cmd.setArgs(["group", "user"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expectedMessage.setDestination("user");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('JoinGroup', () => {
        // GIVEN
        let cmd = new JoinGroup();
        cmd.setArgs(["group"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('KickFromGroup', () => {
        // GIVEN
        let cmd = new KickFromGroup();
        cmd.setArgs(["group", "user", "reason"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expectedMessage.setDestination("user");
        expectedMessage.setReason("reason");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('LeaveGroup', () => {
        // GIVEN
        let cmd = new LeaveGroup();
        cmd.setArgs(["group"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('ListGroupBans', () => {
        // GIVEN
        let cmd = new ListGroupBans();
        cmd.setArgs(["group"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('ListGroupMembers', () => {
        // GIVEN
        let cmd = new ListGroupMembers();
        cmd.setArgs(["group"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('ListGroups', () => {
        // GIVEN
        let cmd = new ListGroups();

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('ListGroupStates', () => {
        // GIVEN
        let cmd = new ListGroupStates();
        cmd.setArgs(["group"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('ListMessages', () => {
        // GIVEN
        let cmd = new ListMessages();
        cmd.setArgs(["group"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('ListUsers', () => {
        // GIVEN
        let cmd = new ListUsers();

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('Quit', () => {
        // GIVEN
        let cmd = new Quit();

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('RemoveGroupAdmin', () => {
        // GIVEN
        let cmd = new RemoveGroupAdmin();
        cmd.setArgs(["group", "user"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expectedMessage.setDestination("user");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('SendPrivately', () => {
        // GIVEN
        let cmd = new SendPrivately();
        cmd.setArgs(["user", "message"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setDestination("user");
        expectedMessage.setMessage("message");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });

    it('UnbanFromGroup', () => {
        // GIVEN
        let cmd = new UnbanFromGroup();
        cmd.setArgs(["group", "user"]);

        // WHEN // THEN: not throwing error.
        cmd.validateArguments();

        // WHEN
        let message = cmd.toMessage();

        // THEN
        let expectedMessage = new Message(cmd.getAction());
        expectedMessage.setGroup("group");
        expectedMessage.setDestination("user");
        expect(message.toJson()).to.equal(expectedMessage.toJson());
    });
});