import {createInterface} from "readline";
import UserCommandFacade from "../../src/client/UserCommandFacade";
import Command from "../../src/client/Command";
import Quit from "../../src/client/commands/Quit";
import CommandListener from "../../src/client/CommandListener";

let expect = require('chai').expect;
let lineReader = createInterface({input: process.stdin, output: process.stdout});
let cmdManager: UserCommandFacade = new UserCommandFacade(lineReader);

describe('UserCommandFacade', () => {
    it('listenOnUserInputs', () => {
        // GIVEN
        cmdManager.addListener(new class implements CommandListener {
            onCommand(cmd: Command): void {
                // THEN
                expect(cmd).instanceof(Quit);
            }
        });

        // WHEN
        cmdManager.listenOnUserInputs();
        lineReader.emit('line', 'q;');
    });
});

after(() => cmdManager.close());