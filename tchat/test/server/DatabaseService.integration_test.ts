import DatabaseService from "../../src/server/persistent/DatabaseService";
import User from "../../src/common/User";
import Group from "../../src/server/Group";
import {Role} from "../../src/server/Role";
import {expect} from "chai";
import Message from "../../src/common/Message";

let db = new DatabaseService(() => {
    console.log("Loaded database!");
});

describe('Database Service', () => {
    it('users', async () => {
        // GIVEN
        let user = new User("1", "123456");

        // WHEN
        await db.upsertUser(user).then(saved => {
            expect(saved).to.equal(true);
        });

        // THEN
        await db.getUser(user.getId()).then(loaded => {
            expect(loaded.toJson()).to.equal(user.toJson());
        })
    });

    it('groups', async () => {
        // GIVEN
        let user = new User("1", "123456");
        let banned = new User("2", "123456");
        let group = new Group("1", true);
        group.putUser(user.getId(), Role.OWNER);
        group.putUser(banned.getId(), Role.MEMBER);
        group.banUser(banned.getId());

        // WHEN
        await db.upsertUser(user).then(saved => {
            expect(saved).to.equal(true);
        });
        await db.upsertUser(banned).then(saved => {
            expect(saved).to.equal(true);
        });
        await db.upsertGroup(group).then(saved => {
            expect(saved).to.equal(true);
        });

        // THEN
        await db.getGroup(group.getId()).then(loaded => {
            expect([user.getId(), banned.getId()]).to.eql(loaded.getUserIds());
            expect([banned.getId()]).to.eql(loaded.getBannedUserIds());
        });

        // THEN
        await db.getAllGroups().then(groups => {
            expect(groups.has(group.getId())).to.eql(true);
            let loaded = groups.get(group.getId());
            expect([user.getId(), banned.getId()]).to.eql(loaded.getUserIds());
            expect([banned.getId()]).to.eql(loaded.getBannedUserIds());
        });
    });

    it('messages', async () => {
        // GIVEN
        let user = new User("1", "123456");
        let group = new Group("1", true);
        group.putUser(user.getId(), Role.OWNER);
        let message = new Message("test", user.getId(), "abc", group.getId());

        // WHEN
        await db.upsertUser(user).then(saved => {
            expect(saved).to.equal(true);
        });
        await db.upsertGroup(group).then(saved => {
            expect(saved).to.equal(true);
        });
        await db.upsertMessage(message).then(saved => {
            expect(saved).to.equal(true);
        });

        // THEN
        await db.getAllGroupsStates().then(states => {
            let loaded = Array.from(states.values())[0];
            expect(loaded[0].getId()).to.not.equal(null);
            expect(message.getAction()).to.equal(loaded[0].getAction());
            expect(message.getGroup()).to.equal(loaded[0].getGroup());
        });

        await db.getStatesOfGroup(group.getId()).then(loaded => {
            expect(loaded[0].getId()).to.not.equal(null);
            expect(message.getAction()).to.equal(loaded[0].getAction());
            expect(message.getGroup()).to.equal(loaded[0].getGroup());
        });

        // THEN
        await db.getMessagesOfUser(user.getId()).then(loaded => {
            expect(loaded[0].getId()).to.not.equal(null);
            expect(message.getAction()).to.equal(loaded[0].getAction());
            expect(message.getSender()).to.equal(loaded[0].getSender());
        });
    });
});