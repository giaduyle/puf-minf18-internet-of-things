import User from "../../src/common/User";

let expect = require('chai').expect;

describe('User', () => {
    it('toJSON', () => {
        // GIVEN
        let user = new User("123", "pass");

        // WHEN
        let json = user.toJson();

        // THEN
        expect(json).to.equal(JSON.stringify({id: "123", password: "pass"}));
    });

    it('fromJSON', () => {
        // GIVEN
        let json = JSON.parse(JSON.stringify({id: "123", password: "pass"}));

        // WHEN
        let user = User.fromJson(json);

        // THEN
        let expectedUser = new User("123", "pass");
        expect(user.getId()).to.equal(expectedUser.getId());
        expect(user.getPassword()).to.equal(expectedUser.getPassword());
    })
});