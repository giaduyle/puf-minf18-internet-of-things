import {expect} from "chai";
import Security from "../../src/common/Security";

describe('Security', () => {
    it('hashing', async () => {
        // GIVEN
        let firstString = "12345abcdef";
        let secondString = "12345abcdef";

        // WHEN
        let hashed = Security.hashString(firstString);

        // THEN
        expect(Security.compareHashes(secondString, hashed)).to.equal(true);
    });

    it('asymmetric encryption/decryption', async () => {
        // GIVEN
        let alice = Security.createECDHChannel();
        let alicePublicKey = alice.generateKeys(Security.DEFAULT_OUTPUT_CODING);

        let bob = Security.createECDHChannel();
        let bobPublicKey = bob.generateKeys(Security.DEFAULT_OUTPUT_CODING);

        let aliceSecret = alice.computeSecret(bobPublicKey, Security.DEFAULT_OUTPUT_CODING);
        let bobSecret = bob.computeSecret(alicePublicKey, Security.DEFAULT_OUTPUT_CODING);

        // WHEN
        let message = "Some message";
        let encrypted = Security.encrypt(message, aliceSecret);

        // THEN
        let decrypted = Security.decrypt(encrypted, bobSecret);
        expect(message).to.equal(decrypted);
    });
});