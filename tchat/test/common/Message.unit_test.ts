import Message from "../../src/common/Message";

let expect = require('chai').expect;

describe('Message', () => {
    it('toJSON', () => {
        // GIVEN
        let message =
            new Message("a1", "s1", "ms1", "gr1", "dest1", 1, "rs1");

        // WHEN
        let json = message.toJson();

        // THEN
        expect(json).to.equal(JSON.stringify({
                sender: "s1",
                action: "a1",
                msg: "ms1",
                group: "gr1",
                destination: "dest1",
                id: 1,
                reason: "rs1"
            }
        ));
    });

    it('fromJSON', () => {
        // GIVEN
        let json = JSON.parse(
            JSON.stringify({
                    sender: "s1",
                    action: "a1",
                    msg: "ms1",
                    group: "gr1",
                    destination: "dest1",
                    id: 1,
                    reason: "rs1"
                }
            )
        );

        // WHEN
        let message = Message.fromJson(json);

        // THEN
        let expectedMessage =
            new Message("a1", "s1", "ms1", "gr1", "dest1", 1, "rs1");
        expect(message.getAction()).to.equal(expectedMessage.getAction());
        expect(message.getSender()).to.equal(expectedMessage.getSender());
        expect(message.getMessage()).to.equal(expectedMessage.getMessage());
        expect(message.getGroup()).to.equal(expectedMessage.getGroup());
        expect(message.getDestination()).to.equal(expectedMessage.getDestination());
        expect(message.getId()).to.equal(expectedMessage.getId());
        expect(message.getReason()).to.equal(expectedMessage.getReason());
    })
});